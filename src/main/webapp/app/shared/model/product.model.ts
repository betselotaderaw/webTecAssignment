import { IInventory } from 'app/shared/model/inventory.model';
import { IReview } from 'app/shared/model/review.model';
import { IShoppingCart } from 'app/shared/model/shopping-cart.model';
import { ICategory } from 'app/shared/model/category.model';

export interface IProduct {
  id?: number;
  name?: string;
  description?: string | null;
  price?: number;
  imageUrl?: string | null;
  stockQuantity?: number;
  inventories?: IInventory[] | null;
  reviews?: IReview[] | null;
  shoppingCart?: IShoppingCart | null;
  category?: ICategory | null;
}

export const defaultValue: Readonly<IProduct> = {};
