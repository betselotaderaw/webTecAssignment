import { ICustomer } from 'app/shared/model/customer.model';

export interface IShoppingCart {
  id?: number;
  customer?: ICustomer | null;
}

export const defaultValue: Readonly<IShoppingCart> = {};
