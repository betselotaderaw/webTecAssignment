import { IOrder } from 'app/shared/model/order.model';

export interface IPayment {
  id?: number;
  order?: IOrder | null;
}

export const defaultValue: Readonly<IPayment> = {};
