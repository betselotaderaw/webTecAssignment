export interface IAdminUser {
  id?: number;
}

export const defaultValue: Readonly<IAdminUser> = {};
