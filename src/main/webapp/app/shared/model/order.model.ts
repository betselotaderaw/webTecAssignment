import dayjs from 'dayjs';
import { IPayment } from 'app/shared/model/payment.model';
import { IShipping } from 'app/shared/model/shipping.model';
import { ICustomer } from 'app/shared/model/customer.model';

export interface IOrder {
  id?: number;
  orderNumber?: string;
  orderDate?: string;
  payment?: IPayment | null;
  shipping?: IShipping | null;
  customer?: ICustomer | null;
}

export const defaultValue: Readonly<IOrder> = {};
