import { IOrder } from 'app/shared/model/order.model';
import { IReview } from 'app/shared/model/review.model';

export interface ICustomer {
  id?: number;
  firstName?: string;
  lastName?: string;
  email?: string;
  shippingAddress?: string | null;
  orders?: IOrder[] | null;
  reviews?: IReview[] | null;
}

export const defaultValue: Readonly<ICustomer> = {};
