import { IOrder } from 'app/shared/model/order.model';

export interface IShipping {
  id?: number;
  order?: IOrder | null;
}

export const defaultValue: Readonly<IShipping> = {};
