import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './shipping.reducer';

export const ShippingDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const shippingEntity = useAppSelector(state => state.shipping.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="shippingDetailsHeading">
          <Translate contentKey="eCommerceApp.shipping.detail.title">Shipping</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="eCommerceApp.shipping.id">Id</Translate>
            </span>
          </dt>
          <dd>{shippingEntity.id}</dd>
          <dt>
            <Translate contentKey="eCommerceApp.shipping.order">Order</Translate>
          </dt>
          <dd>{shippingEntity.order ? shippingEntity.order.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/shipping" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/shipping/${shippingEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default ShippingDetail;
