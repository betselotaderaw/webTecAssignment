import adminUser from 'app/entities/admin-user/admin-user.reducer';
import category from 'app/entities/category/category.reducer';
import customer from 'app/entities/customer/customer.reducer';
import inventory from 'app/entities/inventory/inventory.reducer';
import order from 'app/entities/order/order.reducer';
import payment from 'app/entities/payment/payment.reducer';
import product from 'app/entities/product/product.reducer';
import review from 'app/entities/review/review.reducer';
import shipping from 'app/entities/shipping/shipping.reducer';
import shoppingCart from 'app/entities/shopping-cart/shopping-cart.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

const entitiesReducers = {
  adminUser,
  category,
  customer,
  inventory,
  order,
  payment,
  product,
  review,
  shipping,
  shoppingCart,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
};

export default entitiesReducers;
