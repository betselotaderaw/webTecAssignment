import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import AdminUser from './admin-user';
import AdminUserDetail from './admin-user-detail';
import AdminUserUpdate from './admin-user-update';
import AdminUserDeleteDialog from './admin-user-delete-dialog';

const AdminUserRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<AdminUser />} />
    <Route path="new" element={<AdminUserUpdate />} />
    <Route path=":id">
      <Route index element={<AdminUserDetail />} />
      <Route path="edit" element={<AdminUserUpdate />} />
      <Route path="delete" element={<AdminUserDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default AdminUserRoutes;
