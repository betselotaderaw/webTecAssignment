import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import AdminUser from './admin-user';
import Category from './category';
import Customer from './customer';
import Inventory from './inventory';
import Order from './order';
import Payment from './payment';
import Product from './product';
import Review from './review';
import Shipping from './shipping';
import ShoppingCart from './shopping-cart';
/* jhipster-needle-add-route-import - JHipster will add routes here */

export default () => {
  return (
    <div>
      <ErrorBoundaryRoutes>
        {/* prettier-ignore */}
        <Route path="admin-user/*" element={<AdminUser />} />
        <Route path="category/*" element={<Category />} />
        <Route path="customer/*" element={<Customer />} />
        <Route path="inventory/*" element={<Inventory />} />
        <Route path="order/*" element={<Order />} />
        <Route path="payment/*" element={<Payment />} />
        <Route path="product/*" element={<Product />} />
        <Route path="review/*" element={<Review />} />
        <Route path="shipping/*" element={<Shipping />} />
        <Route path="shopping-cart/*" element={<ShoppingCart />} />
        {/* jhipster-needle-add-route-path - JHipster will add routes here */}
      </ErrorBoundaryRoutes>
    </div>
  );
};
