package com.assign.ecomm.web.rest;

import com.assign.ecomm.domain.Shipping;
import com.assign.ecomm.repository.ShippingRepository;
import com.assign.ecomm.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.assign.ecomm.domain.Shipping}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ShippingResource {

    private final Logger log = LoggerFactory.getLogger(ShippingResource.class);

    private static final String ENTITY_NAME = "shipping";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShippingRepository shippingRepository;

    public ShippingResource(ShippingRepository shippingRepository) {
        this.shippingRepository = shippingRepository;
    }

    /**
     * {@code POST  /shippings} : Create a new shipping.
     *
     * @param shipping the shipping to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shipping, or with status {@code 400 (Bad Request)} if the shipping has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shippings")
    public ResponseEntity<Shipping> createShipping(@RequestBody Shipping shipping) throws URISyntaxException {
        log.debug("REST request to save Shipping : {}", shipping);
        if (shipping.getId() != null) {
            throw new BadRequestAlertException("A new shipping cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Shipping result = shippingRepository.save(shipping);
        return ResponseEntity
            .created(new URI("/api/shippings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shippings/:id} : Updates an existing shipping.
     *
     * @param id the id of the shipping to save.
     * @param shipping the shipping to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shipping,
     * or with status {@code 400 (Bad Request)} if the shipping is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shipping couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shippings/{id}")
    public ResponseEntity<Shipping> updateShipping(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Shipping shipping
    ) throws URISyntaxException {
        log.debug("REST request to update Shipping : {}, {}", id, shipping);
        if (shipping.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, shipping.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!shippingRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Shipping result = shippingRepository.save(shipping);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, shipping.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /shippings/:id} : Partial updates given fields of an existing shipping, field will ignore if it is null
     *
     * @param id the id of the shipping to save.
     * @param shipping the shipping to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shipping,
     * or with status {@code 400 (Bad Request)} if the shipping is not valid,
     * or with status {@code 404 (Not Found)} if the shipping is not found,
     * or with status {@code 500 (Internal Server Error)} if the shipping couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/shippings/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Shipping> partialUpdateShipping(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Shipping shipping
    ) throws URISyntaxException {
        log.debug("REST request to partial update Shipping partially : {}, {}", id, shipping);
        if (shipping.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, shipping.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!shippingRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Shipping> result = shippingRepository
            .findById(shipping.getId())
            .map(existingShipping -> {
                return existingShipping;
            })
            .map(shippingRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, shipping.getId().toString())
        );
    }

    /**
     * {@code GET  /shippings} : get all the shippings.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shippings in body.
     */
    @GetMapping("/shippings")
    public List<Shipping> getAllShippings() {
        log.debug("REST request to get all Shippings");
        return shippingRepository.findAll();
    }

    /**
     * {@code GET  /shippings/:id} : get the "id" shipping.
     *
     * @param id the id of the shipping to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shipping, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shippings/{id}")
    public ResponseEntity<Shipping> getShipping(@PathVariable Long id) {
        log.debug("REST request to get Shipping : {}", id);
        Optional<Shipping> shipping = shippingRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(shipping);
    }

    /**
     * {@code DELETE  /shippings/:id} : delete the "id" shipping.
     *
     * @param id the id of the shipping to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shippings/{id}")
    public ResponseEntity<Void> deleteShipping(@PathVariable Long id) {
        log.debug("REST request to delete Shipping : {}", id);
        shippingRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
