package com.assign.ecomm.repository;

import com.assign.ecomm.domain.AdminUser;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the AdminUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdminUserRepository extends JpaRepository<AdminUser, Long> {}
